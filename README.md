# DesQ Term
## A simple terminal emulator for the DesQ Desktop Environment

DesQ Term is a simple terminal emulator built on top of the powerful Qt widget toolkit, with minimal features.

Some of its features are

* Fast startup
* Set custom shell command
* Set terminal color themes and opacity
* Set terminal font


### Dependencies
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* QTermWidget (libqtermwidget5-0-dev)
* libdesq (https://gitlab.com/DesQ/libdesq)
* libdesqui (https://gitlab.com/DesQ/libdesqui)
* DFL::Application (https://gitlab.com/desktop-frameworks/application)
* DFL::Settings (https://gitlab.com/desktop-frameworks/settings)
* DFL::Utils (https://gitlab.com/desktop-frameworks/utils)


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQApps/DesQTerm.git DesQTerm`
- Enter the `DesQTerm` folder
  * `cd DesQTerm`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Upcoming
* Any other feature you request for... :)
