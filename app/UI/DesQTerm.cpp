/**
 * Copyright 2012-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "DesQTerm.hpp"
#include "TermWidget.hpp"
#include "SettingsDialog.hpp"

DesQTerm::DesQTerm( QString wdir, QString cmd, bool mono ) : QMainWindow() {
    wDir = wdir;
    exec = cmd;

    if ( mono ) {
        createMonoTerm();
    }

    else {
        createGUI();
        setupActions();
    }

    setWindowProperties();
}


DesQTerm::~DesQTerm() {
    if ( tabWidget ) {
        for ( int i = 0; i < tabWidget->count(); i++ ) {
            TermWidget *tw = qobject_cast<TermWidget *>( tabWidget->widget( i ) );
            tw->sendText( QString::fromLocal8Bit( "exit\n" ) );
        }
    }
}


void DesQTerm::show() {
    bool maximized = settings->value( "Session::ShowMaximized" );

    if ( maximized ) {
        QMainWindow::showMaximized();
    }

    else {
        QMainWindow::showNormal();
    }

    qApp->processEvents();
    QMainWindow::show();
}


void DesQTerm::handleMessages( QString msg, int fd ) {
    Q_UNUSED( fd );

    QStringList parts = msg.split( "\n", Qt::KeepEmptyParts );

    if ( parts[ 0 ] == "new-window" ) {
        DesQTerm *gui = new DesQTerm( parts[ 1 ], parts[ 2 ], false );
        gui->show();
    }
}


void DesQTerm::createGUI() {
    /* Our tab widget */
    tabWidget = new TabWidget( this, false );
    connect( tabWidget, &TabWidget::changeWindowTitle, this, &QMainWindow::setWindowTitle );

    /* Base Layout */
    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setSpacing( 0 );
    lyt->setContentsMargins( QMargins() );
    lyt->addWidget( tabWidget );

    /* Base Widget */
    QWidget *widget = new QWidget();

    widget->setObjectName( "base" );
    widget->setLayout( lyt );

    setCentralWidget( widget );

    /* Start a terminal session */
    startTerminal();

    /* Widget Properties */
    tabWidget->setFocusPolicy( Qt::NoFocus );
}


void DesQTerm::createMonoTerm() {
    /* Setup our term widget */
    if ( wDir.isEmpty() and exec.isEmpty() ) {
        monoTerm = new TermWidget( this );
    }

    else {
        monoTerm = new TermWidget( wDir, exec, this );
    }

    connect(
        monoTerm, &TermWidget::titleChanged, [ = ]() {
            setWindowTitle( monoTerm->title() );
        }
    );

    /* Base Layout */
    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setSpacing( 0 );
    lyt->setContentsMargins( QMargins() );
    lyt->addWidget( monoTerm );

    /* Base Widget */
    QWidget *widget = new QWidget();

    widget->setObjectName( "base" );
    widget->setLayout( lyt );

    setCentralWidget( widget );

    // Clear Terminal
    QAction *clearTermAct = new QAction( "C&lear Terminal", this );

    clearTermAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+X" ) );

    connect( clearTermAct, &QAction::triggered, monoTerm, &TermWidget::clear );
    addAction( clearTermAct );

    // Copy Selection
    QAction *copyAct = new QAction( QIcon( ":/icons/edit-copy.png" ), "&Copy", this );

    copyAct->setShortcut( tr( "Ctrl+Shift+C" ) );

    connect( copyAct, &QAction::triggered, monoTerm, &TermWidget::copyClipboard );
    addAction( copyAct );

    // Paste Clipboard
    QAction *pasteAct = new QAction( QIcon( ":/icons/edit-paste.png" ), "&Paste", this );

    pasteAct->setShortcut( tr( "Ctrl+Shift+V" ) );

    connect( pasteAct, &QAction::triggered, monoTerm, &TermWidget::pasteClipboard );
    addAction( pasteAct );

    // Terminal Settings
    QAction *settingsAct = new QAction( "&Settings", this );

    settingsAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+S" ) );

    connect( settingsAct, &QAction::triggered, this, &DesQTerm::showSettings );
    addAction( settingsAct );

    // Open FileManager here
    QAction *fmgrAct = new QAction( "Open &File Manager", this );

    fmgrAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+O" ) );

    connect( fmgrAct, &QAction::triggered, this, &DesQTerm::openFMgr );
    addAction( fmgrAct );

    // Close the terminal when the process ends
    connect( monoTerm, &TermWidget::finished, this, &DesQTerm::close );
}


void DesQTerm::startTerminal() {
    if ( wDir.isEmpty() and exec.isEmpty() ) {
        tabWidget->newTerminal();
    }

    else {
        tabWidget->newTerminal( wDir, exec );
    }
}


void DesQTerm::setupActions() {
    connect( settings,  &DFL::Settings::settingChanged, this, &DesQTerm::reloadSettings );

    connect( tabWidget, SIGNAL(close()),                this, SLOT( close() ) );

    // New Terminal
    QAction *newTermAct = new QAction( "&New Terminal", this );

    newTermAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+N" ) );

    connect( newTermAct, SIGNAL(triggered()), tabWidget, SLOT(newTerminal()) );
    addAction( newTermAct );

    // New Terminal in the current  directory
    QAction *newTermCwdAct = new QAction( "&New Terminal in CWD", this );

    newTermCwdAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+T" ) );

    connect( newTermCwdAct, SIGNAL(triggered()), tabWidget, SLOT(newTerminalCWD()) );
    addAction( newTermCwdAct );

    // Clear Terminal
    QAction *clearTermAct = new QAction( "C&lear Terminal", this );

    clearTermAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+X" ) );

    connect( clearTermAct, SIGNAL(triggered()), tabWidget, SLOT(clearTerminal()) );
    addAction( clearTermAct );

    // Copy Selection
    QAction *copyAct = new QAction( QIcon( ":/icons/edit-copy.png" ), "&Copy", this );

    copyAct->setShortcut( tr( "Ctrl+Shift+C" ) );

    connect( copyAct, SIGNAL(triggered()), tabWidget, SLOT(copyToClipboard()) );
    addAction( copyAct );

    // Paste Clipboard
    QAction *pasteAct = new QAction( QIcon( ":/icons/edit-paste.png" ), "&Paste", this );

    pasteAct->setShortcut( tr( "Ctrl+Shift+V" ) );

    connect( pasteAct, SIGNAL(triggered()), tabWidget, SLOT(pasteClipboard()) );
    addAction( pasteAct );

    // Previous Terminal
    QAction *prevTermAct = new QAction( "&prev Terminal", this );

    prevTermAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+Tab" ) << tr( "Ctrl+PgUp" ) << tr( "Shift+Left" ) );

    connect( prevTermAct, SIGNAL(triggered()), tabWidget, SLOT(prevTerminal()) );
    addAction( prevTermAct );

    // Next Terminal
    QAction *nextTermAct = new QAction( "&Next Terminal", tabWidget );

    nextTermAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Tab" ) << tr( "Ctrl+PgDown" ) << tr( "Shift+Right" ) );

    connect( nextTermAct, SIGNAL(triggered()), tabWidget, SLOT(nextTerminal()) );
    addAction( nextTermAct );

    // Terminal Settings
    QAction *settingsAct = new QAction( "&Settings", tabWidget );

    settingsAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+S" ) );

    connect( settingsAct, SIGNAL(triggered()), this, SLOT(showSettings()) );
    addAction( settingsAct );

    // Open FileManager here
    QAction *fmgrAct = new QAction( "Open &File Manager", this );

    fmgrAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+O" ) );

    connect( fmgrAct, SIGNAL(triggered()), this, SLOT(openFMgr()) );
    addAction( fmgrAct );

    // closeTab DesQTerm
    QAction *closeTabAct = new QAction( "Close &Tab", this );

    closeTabAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+W" ) );

    connect( closeTabAct, SIGNAL(triggered()), tabWidget, SLOT(closeTab()) );
    addAction( closeTabAct );

    // Quit DesQTerm
    QAction *quitAct = new QAction( "&Quit", this );

    quitAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+Q" ) );

    connect( quitAct, SIGNAL(triggered()), this, SLOT(close()) );
    addAction( quitAct );
}


void DesQTerm::setWindowProperties() {
    setWindowTitle( "DesQTerm" );
    setWindowIcon( QIcon( ":/icons/desq-term.png" ) );

    setGeometry( settings->value( "Session::Geometry" ) );
    setMinimumSize( 400, 300 );

    /* No Title Bar */
    Qt::WindowFlags flags = Qt::Window;

    if ( settings->value( "Borderless" ) ) {
        flags |= Qt::FramelessWindowHint;
    }

    setWindowFlags( flags );

    /* Transparency */
    if ( settings->value( "EnableTransparency" ) ) {
        setAttribute( Qt::WA_TranslucentBackground );
        mAlpha = (int)settings->value( "Transparency" );
    }
}


void DesQTerm::showSettings() {
    SettingsDialog *settingsDlg = new SettingsDialog();

    settingsDlg->exec();

    Qt::WindowFlags flags = Qt::Window;

    if ( settings->value( "Borderless" ) ) {
        flags |= Qt::FramelessWindowHint;
    }

    setWindowFlags( flags );

    show();
}


void DesQTerm::reloadSettings( QString key, QVariant value ) {
    if ( key == "EnableTransparency" ) {
        setAttribute( Qt::WA_TranslucentBackground, value.toBool() );
    }

    else if ( key == "Transparency" ) {
        mAlpha = value.toInt();
        repaint();
    }

    else if ( key == "TabPosition" ) {
        qCritical() << tabWidget << value.toInt();

        if ( tabWidget ) {
            tabWidget->setTabPosition( (QTabWidget::TabPosition)value.toInt() );
        }
    }

    else {
        // if ( tabWidget->count() ) {
        // TermWidget *term = qobject_cast<TermWidget *>( tabWidget->widget( 0 ) );
        // QSize colRow = term->
        // }

        // if ( key == "Font" ) {
        // QFont f( value.value<QFont>() );
        // }

        for ( int i = 0; i < tabWidget->count(); i++ ) {
            TermWidget *term = qobject_cast<TermWidget *>( tabWidget->widget( i ) );
            term->reloadSettings( key, value );
        }
    }
}


void DesQTerm::openFMgr() {
    QString cwd = qobject_cast<TermWidget *>( tabWidget->currentWidget() )->currentWorkingDirectory();

    QProcess::startDetached( "xdg-open", QStringList() << cwd );
}


void DesQTerm::closeEvent( QCloseEvent *cEvent ) {
    if ( tabWidget ) {
        /** Collect the term widgets */
        QList<TermWidget *> terms;
        for ( int i = 0; i < tabWidget->count(); i++ ) {
            terms << qobject_cast<TermWidget *>( tabWidget->widget( i ) );
        }

        /** Delete the widgets one by one */
        for ( TermWidget *term: terms ) {
            if ( term and term->hasForegroundProcess() ) {
                int reply = QMessageBox::question(
                    this,
                    "DesQ Term | Close Tab?",
                    "You have a process running in the foreground. Closing this tab will kill the process. "
                    "Kill the process and close tab?",
                    QMessageBox::Yes | QMessageBox::No
                );

                if ( reply == QMessageBox::No ) {
                    continue;
                }
            }

            delete term;
        }

        if ( tabWidget->count() ) {
            cEvent->ignore();
            return;
        }
    }

    else {
        if ( monoTerm->hasForegroundProcess() ) {
            int reply = QMessageBox::question(
                this,
                "DesQ Term | Close Terminal?",
                "You have a process running in the foreground. Closing this terminal will kill the process. "
                "Kill the process and close terminal?",
                QMessageBox::Yes | QMessageBox::No
            );

            if ( reply == QMessageBox::No ) {
                cEvent->ignore();
                return;
            }
        }
    }

    settings->setValue( "Session::ShowMaximized", isMaximized() );

    /** Save the geometry only if the window is not maximized */
    if ( not isMaximized() ) {
        settings->setValue( "Session::Geometry", geometry() );
    }

    cEvent->accept();
}


void DesQTerm::paintEvent( QPaintEvent *pEvent ) {
    if ( mAlpha != 100 ) {
        QPainter painter( this );
        painter.setPen( Qt::NoPen );
        painter.setBrush( QColor( 0, 0, 0, 255 - 255 * mAlpha / 100 ) );
        painter.drawRect( rect() );
        painter.end();
    }

    QMainWindow::paintEvent( pEvent );

    if ( (bool)settings->value( "Borderless" ) ) {
        QPainter painter( this );
        painter.setPen( Qt::gray );
        painter.drawRect( rect().adjusted( 0, 0, -1, -1 ) );
        painter.end();
    }
}
