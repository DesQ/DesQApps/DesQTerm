/**
 * Copyright 2012-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <desq/desq-config.h>
#include <desq/Utils.hpp>

#include <DFXdg.hpp>
#include <DFUtils.hpp>
#include <DFSettings.hpp>
#include <DFApplication.hpp>

#include "DesQTerm.hpp"
#include "TermWidget.hpp"
#include "SettingsDialog.hpp"

#include <unistd.h>
#include <sys/utsname.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <errno.h>

DFL::Settings *settings;

static inline const char * platform() {
    struct utsname buffer;

    uname( &buffer );

    QString QtVersion  = QString( "Powered by Qt " ) + QT_VERSION_STR + " on Linux " + buffer.release;
    QString GCCVersion = QString( "Compiled with GCC " ) + __VERSION__;

    QString version = QtVersion + '\n' + GCCVersion;

    return qPrintable( version );
}


int main( int argc, char **argv ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Term.log" ).toLocal8Bit().data(), "a" );
    qInstallMessageHandler( DFL::Logger );

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Docs started at" << QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8().constData();
    qDebug() << "------------------------------------------------------------------------";

    DFL::Application app( argc, argv );

    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "Term" );
    app.setApplicationVersion( PROJECT_VERSION );
    app.setDesktopFileName( "desq-term" );

    app.interceptSignal( SIGTERM, nullptr );

    QCommandLineParser parser;
    parser.addOption( { { "h", "help" }, "Print this help" } );
    parser.addOption( { { "v", "version" }, "Print application version and exit" } );
    parser.addOption( { { "m", "monoterm" }, "Open a single terminal without tabs" } );
    parser.addOption( { { "w", "workdir" }, "Start session with specified work directory", "workdir" } );
    parser.addOption( { { "e", "execute" }, "Start session with specified program as shell", "execute" } );
    parser.addOption( { { "s", "settings" }, "Open only the settings dialog" } );
    parser.addOption( { { "u", "unique" }, "Start a unique session, not connected to existing instance" } );

    /** Process the CLI arguments */
    parser.process( app );

    /** == Help and Version == **/
    if ( parser.isSet( "help" ) ) {
        std::cout << "DesQ Term v" << PROJECT_VERSION << "\n" << std::endl;
        std::cout << "Usage: DesQ Term [options]" << std::endl;
        std::cout << "Options:" << std::endl;
        std::cout << "  -h|--help               Print this help" << std::endl;
        std::cout << "  -v|--version            Prints application version and exit" << std::endl;
        std::cout << "  -m|--monoterm           Open a single terminal without tabs" << std::endl;
        std::cout << "  -w|--workdir <dir>      Start session with specified work directory" << std::endl;
        std::cout << "  -e|--execute <command>  Execute command in the shell" << std::endl;
        std::cout << "  -s|--settings           Show DesQ Term settings dialog" << std::endl;
        std::cout << "  -u|--unique             Open this instance as a separate process\n" << std::endl;
        std::cout << "Feature requests, bug reports etc please send to: <marcusbritanicus@gmail.com>\n" << std::endl;

        return 0;
    }

    if ( parser.isSet( "version" ) ) {
        std::cout << "DesQ Term " << PROJECT_VERSION << std::endl;
        std::cout << platform() << std::endl;

        return 0;
    }

    /** == GUI == **/

    /** Initialize @settings object */
    settings = DesQ::Utils::initializeDesQSettings( "Term", "Term" );

    /** Settings Dialog */
    if ( parser.isSet( "settings" ) ) {
        SettingsDialog *settingsDlg = new SettingsDialog();
        settingsDlg->exec();

        return 0;
    }

    /** App */
    else {
        QString workDir( parser.value( "workdir" ) );

        /**
         * Attempt to set the working directory from the environment. We'll use QDir::currentPath()
         * Dolphin etc rely on this, and do not pass --workdir, or -w switches
         */
        if ( workDir.isEmpty() ) {
            workDir = QDir::currentPath();
        }

        QString execute( parser.value( "execute" ) );

        /** Monoterm will always be a separate process. */
        if ( parser.isSet( "monoterm" ) ) {
            DesQTerm Gui( workDir, execute, true );

            Gui.show();

            /** Start the event loop, and exit to the calling process at the end of it. */
            return app.exec();
        }

        /** Explicit request for new process */
        else if ( parser.isSet( "unique" ) ) {
            DesQTerm Gui( workDir, execute, false );

            Gui.show();

            /** Start the event loop, and exit to the calling process at the end of it. */
            return app.exec();
        }

        /** User wants a single instance */
        if ( (bool)settings->value( "UseSingleInstance" ) ) {
            /** Another instance is already running */
            if ( app.isRunning() ) {
                app.messageServer( QString( "new-window\n%1\n%2" ).arg( workDir ).arg( execute ) );

                return 0;
            }

            if ( app.lockApplication() == false ) {
                qWarning() << "Failed to lock instance. Multiple isntances may start.";
            }

            else {
                qInfo() << "Single instance created";
            }
        }

        /** Gui instance. */
        DesQTerm Gui( workDir, execute, false );

        Gui.show();

        /**
         * This is the first instance of our app. Watch for signals from clients.
         */
        QObject::connect( &app, &DFL::Application::messageFromClient, &Gui, &DesQTerm::handleMessages );

        return app.exec();
    }

    return 0;
}
