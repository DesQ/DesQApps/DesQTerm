/**
 * Copyright 2012-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "Terminal.hpp"
#include "TermWidget.hpp"
#include "SettingsDialog.hpp"

#include <desq/Utils.hpp>
#include <desq/desq-config.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <errno.h>

DFL::Settings *settings;

TerminalWidget::TerminalWidget( QWidget *parent ) : QWidget( parent ) {
    settings = DesQ::Utils::initializeDesQSettings( "Term", "Term" );

    createGUI();
    setupActions();
    setWindowProperties();

    tabWidget->currentWidget()->setFocus();
}


void TerminalWidget::createGUI() {
    tabWidget = new TabWidget( this, true );
    tabWidget->setTabPosition( QTabWidget::South );
    connect( tabWidget, &TabWidget::changeWindowTitle, this, &QWidget::setWindowTitle );

    /* Base Layout */
    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setSpacing( 0 );
    lyt->setContentsMargins( QMargins() );
    lyt->addWidget( tabWidget );

    tabWidget->newTerminal();

    setLayout( lyt );
}


void TerminalWidget::setupActions() {
    connect( settings, &DFL::Settings::settingChanged, this, &TerminalWidget::reloadSettings );

    // New Terminal
    QAction *newTermAct = new QAction( "&New Terminal", this );

    newTermAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+N" ) );

    connect( newTermAct, SIGNAL(triggered()), tabWidget, SLOT(newTerminal()) );
    addAction( newTermAct );

    // New Terminal in the current  directory
    QAction *newTermCwdAct = new QAction( "&New Terminal in CWD", this );

    newTermCwdAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+T" ) );

    connect( newTermCwdAct, SIGNAL(triggered()), tabWidget, SLOT(newTerminalCWD()) );
    addAction( newTermCwdAct );

    // Clear Terminal
    QAction *clearTermAct = new QAction( "C&lear Terminal", this );

    clearTermAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+X" ) );

    connect( clearTermAct, SIGNAL(triggered()), tabWidget, SLOT(clearTerminal()) );
    addAction( clearTermAct );

    // Copy Selection
    QAction *copyAct = new QAction( QIcon::fromTheme( "edit-copy" ), "&Copy", this );

    copyAct->setShortcut( tr( "Ctrl+Shift+C" ) );

    connect( copyAct, SIGNAL(triggered()), tabWidget, SLOT(copyToClipboard()) );
    addAction( copyAct );

    QAction *pasteAct = new QAction( QIcon::fromTheme( "edit-paste" ), "&Paste", this );

    pasteAct->setShortcut( tr( "Ctrl+Shift+V" ) );

    connect( pasteAct, SIGNAL(triggered()), tabWidget, SLOT(pasteClipboard()) );
    addAction( pasteAct );

    // Previous Terminal
    QAction *prevTermAct = new QAction( "&prev Terminal", this );

    prevTermAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+Tab" ) << tr( "Ctrl+PgUp" ) << tr( "Shift+Left" ) );

    connect( prevTermAct, SIGNAL(triggered()), tabWidget, SLOT(prevTerminal()) );
    addAction( prevTermAct );

    // Next Terminal
    QAction *nextTermAct = new QAction( "&Next Terminal", tabWidget );

    nextTermAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Tab" ) << tr( "Ctrl+PgDown" ) << tr( "Shift+Right" ) );

    connect( nextTermAct, SIGNAL(triggered()), tabWidget, SLOT(nextTerminal()) );
    addAction( nextTermAct );

    // Terminal Settings
    QAction *settingsAct = new QAction( "&Settings", tabWidget );

    settingsAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+S" ) );

    connect( settingsAct, SIGNAL(triggered()), SLOT(showSettings()) );
    addAction( settingsAct );

    // Open FileManager here
    QAction *fmgrAct = new QAction( "Open &File Manager", this );

    fmgrAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+O" ) );

    connect( fmgrAct, SIGNAL(triggered()), SLOT(openFMgr()) );
    addAction( fmgrAct );

    // closeTab TerminalWidget
    QAction *closeTabAct = new QAction( "Close &Tab", this );

    closeTabAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+W" ) );

    connect( closeTabAct, SIGNAL(triggered()), tabWidget, SLOT(closeTab()) );
    addAction( closeTabAct );

    // Quit TerminalWidget
    QAction *quitAct = new QAction( "&Quit", this );

    quitAct->setShortcuts( QList<QKeySequence>() << tr( "Ctrl+Shift+Q" ) );

    connect( quitAct, SIGNAL(triggered()), SLOT(close()) );
    addAction( quitAct );
}


void TerminalWidget::setWindowProperties() {
    setWindowTitle( "TerminalWidget" );
    setWindowIcon( QIcon::fromTheme( "desq-term" ) );

    setAttribute( Qt::WA_TranslucentBackground );

    setWindowFlags( Qt::Dialog | Qt::CustomizeWindowHint );
}


void TerminalWidget::showSettings() {
    SettingsDialog *settingsDlg = new SettingsDialog();

    settingsDlg->exec();

    show();
}


void TerminalWidget::reloadSettings( QString key, QVariant value ) {
    if ( key == "EnableTransparency" ) {
        setAttribute( Qt::WA_TranslucentBackground, value.toBool() );
    }

    else {
        for ( int i = 0; i < tabWidget->count(); i++ ) {
            TermWidget *term = qobject_cast<TermWidget *>( tabWidget->widget( i ) );
            term->reloadSettings( key, value );
        }
    }
}


void TerminalWidget::openFMgr() {
    QString cwd = qobject_cast<TermWidget *>( tabWidget->currentWidget() )->currentWorkingDirectory();

    QProcess::startDetached( "xdg-open", QStringList() << cwd );
}


void TerminalWidget::focusInEvent( QFocusEvent *fEvent ) {
    QWidget::focusInEvent( fEvent );
    qApp->processEvents();

    if ( tabWidget->count() ) {
        tabWidget->currentWidget()->setFocus();
    }
}
