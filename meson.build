project(
	'DesQ Term',
	'cpp',
	version: '0.0.9',
	license: 'GPLv3',
	meson_version: '>=0.59.0',
	default_options: [
		'cpp_std=c++17',
		'c_std=c11',
		'warning_level=2',
		'werror=false',
	],
)

add_global_arguments( '-DPROJECT_VERSION="@0@"'.format( meson.project_version() ), language : 'cpp' )
add_project_arguments( '-Wno-template-id-cdtor', language : 'cpp' )
add_project_link_arguments( ['-rdynamic','-fPIC'], language:'cpp' )

Qt     = import( 'qt6' )
QtDeps = dependency(
	'qt6',
	modules: [ 'Core', 'Core5Compat', 'Gui', 'Widgets' ],
	required: false
)

WayQt       = dependency( 'wayqt-qt6', required: false )
DesQCore    = dependency( 'desq-core-qt6', required: false )
DesQGui     = dependency( 'desq-gui-qt6', required: false )
DesQWidgets = dependency( 'desq-widgets-qt6', required: false )
DFApp       = dependency( 'df6application', required: false )
DFSettings  = dependency( 'df6settings', required: false )
DFUtils     = dependency( 'df6utils', required: false )
DFXDG       = dependency( 'df6xdg', required: false )

QTermWidget = dependency( 'qtermwidget6' )

UTF8Proc = dependency( 'libutf8proc', required: false )
if UTF8Proc.found()
	HAVE_UTF8PROC = '-DHAVE_UTF8PROC'
else
	HAVE_UTF8PROC = ''
endif

Deps = [ QtDeps, DesQCore, DesQGui, DesQWidgets, DFSettings, DFUtils, DFApp, DFXDG, QTermWidget ]

Includes = [
	'common',
	'common/Settings',
	'common/Widgets',
	'app',
	'app/UI',
	'plugin'
]

CommonHeaders = [
	'common/Global.hpp',
	'common/Settings/SettingsDialog.hpp',
	'common/Widgets/TabWidget.hpp',
	'common/Widgets/TermWidget.hpp'
]

CommonSources = [
	'common/Settings/SettingsDialog.cpp',
	'common/Widgets/TabWidget.cpp',
	'common/Widgets/TermWidget.cpp'
]

CommonMocs = Qt.compile_moc(
	headers : CommonHeaders,
	dependencies: Deps
)

common = static_library(
	'common', [ CommonSources, CommonMocs ],
	dependencies: Deps,
	include_directories: Includes,
	install: false,
)

CommonStatic = declare_dependency(
	link_with: common,
	dependencies: Deps
)

AppHeaders = [
	'app/UI/DesQTerm.hpp'
]

AppSources = [
	'app/Main.cpp',
	'app/UI/DesQTerm.cpp'
]

AppMocs = Qt.compile_moc(
	headers : AppHeaders,
	dependencies: Deps
)

Resources = Qt.compile_resources(
	name: 'term_rcc',
	sources: 'resources/DesQTerm.qrc'
)

desqterm = executable(
	'desq-term', [ AppSources, AppMocs, Resources ],
	dependencies: [ Deps, CommonStatic ],
	include_directories: Includes,
	install: true
)

PluginMocs = Qt.compile_moc(
	headers : [ 'plugin/plugin.hpp', 'plugin/Terminal.hpp' ],
	dependencies: Deps,
	include_directories: [ Includes, join_paths( get_option( 'prefix' ), get_option( 'includedir' ) ) ]
)

termplugin = shared_module(
	'term', [ 'plugin/plugin.cpp', 'plugin/Terminal.cpp', PluginMocs ],
	dependencies: [ Deps, CommonStatic ],
	include_directories: [ Includes, '/usr/include' ],
	install: true,
	install_dir: join_paths( get_option( 'libdir' ), 'desq', 'plugins', 'dropdown' )
)

install_data(
	'resources/desq-term.desktop',
	install_dir: join_paths( get_option( 'prefix' ), get_option( 'datadir' ), 'applications' )
)

install_data(
	'resources/Term.conf',
	install_dir: join_paths( get_option( 'prefix' ), get_option( 'datadir' ), 'desq', 'configs' )
)

install_data(
    'README.md', 'Changelog', 'ReleaseNotes',
    install_dir: join_paths( get_option( 'datadir' ), 'desq-term' ),
)

install_data(
    'resources/icons/desq-term.svg',
    install_dir: join_paths( get_option( 'datadir' ), 'icons', 'hicolor', 'scalable', 'apps' ),
)

install_data(
    'resources/icons/desq-term.png',
    install_dir: join_paths( get_option( 'datadir' ), 'icons', 'hicolor', '256x256', 'apps' ),
)

install_subdir(
	'resources/color-schemes',
	install_dir: join_paths( get_option( 'datadir' ), 'desq-term' ),
)

install_subdir(
	'resources/kb-layouts',
	install_dir: join_paths( get_option( 'datadir' ), 'desq-term' ),
)

install_subdir(
	'resources/translations',
	install_dir: join_paths( get_option( 'datadir' ), 'desq-term' ),
)
