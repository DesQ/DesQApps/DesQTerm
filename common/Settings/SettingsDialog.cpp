/**
 * Copyright 2012-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "SettingsDialog.hpp"
#include <desq/desq-config.h>
#include <DFSettings.hpp>
#include <DFUtils.hpp>
#include <qtermwidget.h>

#include "Global.hpp"


SettingsDialog::SettingsDialog() : QDialog() {
    createGUI();
}


void SettingsDialog::createGUI() {
    QLabel *shellLabel  = new QLabel( "She&ll program" );
    QLabel *comboLabel  = new QLabel( "Color &Scheme" );
    QLabel *dspinLabel  = new QLabel( "Tr&ansparency" );
    QLabel *fontLabel   = new QLabel( "&Font" );
    QLabel *cursorLabel = new QLabel( "&Cursor Shape" );
    QLabel *termLabel   = new QLabel( "Ter&m Variable" );
    QLabel *tabPosLabel = new QLabel( "Tab &Position" );

    shellEdit = new QLineEdit( this );
    shellEdit->setText( settings->value( "Shell" ) );
    shellLabel->setBuddy( shellEdit );
    connect(
        shellEdit, &QLineEdit::textEdited, [ = ]( QString sh ) {
            if ( DFL::Utils::isExecutable( sh ) ) {
                settings->setValue( "Shell", sh );
            }
        }
    );

    colorSchemesCombo = new QComboBox();
    colorSchemesCombo->addItems( QTermWidget::availableColorSchemes() );
    colorSchemesCombo->setCurrentIndex( QTermWidget::availableColorSchemes().indexOf( settings->value( "ColorScheme" ) ) );
    connect(
        colorSchemesCombo, &QComboBox::currentTextChanged, [ = ]( QString text ) {
            settings->setValue( "ColorScheme", text );
        }
    );

    transparencySpin = new QSpinBox();
    transparencySpin->setRange( 0, 100 );
    transparencySpin->setSingleStep( 1 );
    transparencySpin->setValue( settings->value( "Transparency" ) );
    connect( transparencySpin, SIGNAL(valueChanged(int)), this, SLOT(setTransparency()) );

    fontCombo = new QFontComboBox();
    QFont font = settings->value( "Font" );

    fontCombo->setCurrentFont( font );
    fontCombo->setFontFilters( QFontComboBox::MonospacedFonts );
    connect( fontCombo, SIGNAL(currentFontChanged(const QFont&)), this, SLOT(setFont(QFont)) );

    fontSizeSpin = new QSpinBox();
    fontSizeSpin->setRange( 0, 72 );
    fontSizeSpin->setSingleStep( 1 );

    QFont tFont = settings->value( "Font" );

    fontSizeSpin->setValue( tFont.pointSize() );
    connect( fontSizeSpin, SIGNAL(valueChanged(int)), this, SLOT(setFont(int)) );

    enableTransparencyCheck = new QCheckBox( "Enable &Transparency" );
    enableTransparencyCheck->setChecked( settings->value( "EnableTransparency" ) );
    connect( enableTransparencyCheck, SIGNAL(toggled(bool)), this, SLOT(setEnableTransparency()) );

    borderlessCheck = new QCheckBox( "&Hide Window Borders (Requires restart)" );
    borderlessCheck->setChecked( settings->value( "Borderless" ) );
    borderlessCheck->setToolTip( "May require restart of all running terminal windows" );
    connect( borderlessCheck, SIGNAL(toggled(bool)), this, SLOT(setBorderless()) );

    singleInstanceCheck = new QCheckBox( "&Use single instance" );
    singleInstanceCheck->setChecked( settings->value( "UseSingleInstance" ) );
    singleInstanceCheck->setToolTip( "Requires restart of all running DesQ Term instances" );
    connect( singleInstanceCheck, SIGNAL(toggled(bool)), this, SLOT(setSingleInstance()) );

    cursorShapeCombo = new QComboBox();
    cursorShapeCombo->addItems( { "Block Cursor", "Underline Cursor", "I-Beam Cursor" } );
    cursorShapeCombo->setCurrentIndex( settings->value( "CursorShape" ) );
    connect(
        cursorShapeCombo, &QComboBox::currentTextChanged, [ = ]() {
            settings->setValue( "CursorShape", cursorShapeCombo->currentIndex() );
        }
    );

    tabPositionCombo = new QComboBox();
    tabPositionCombo->addItems( { "North", "South", "West", "East" } );
    tabPositionCombo->setCurrentIndex( settings->value( "TabPosition" ) );
    connect(
        tabPositionCombo, &QComboBox::currentTextChanged, [ = ]() {
            settings->setValue( "TabPosition", tabPositionCombo->currentIndex() );
        }
    );

    comboLabel->setBuddy( colorSchemesCombo );
    dspinLabel->setBuddy( transparencySpin );
    fontLabel->setBuddy( fontCombo );
    cursorLabel->setBuddy( cursorShapeCombo );
    tabPosLabel->setBuddy( tabPositionCombo );

    termEdit = new QLineEdit();
    termEdit->setText( settings->value( "Term" ) );
    termLabel->setBuddy( termEdit );
    connect(
        termEdit, &QLineEdit::textEdited, [ = ]( QString term ) {
            settings->setValue( "Term", term );
        }
    );

    QPushButton *okBtn = new QPushButton( QIcon::fromTheme( "dialog-close" ), tr( "&Close" ) );

    connect( okBtn, SIGNAL(clicked()), this, SLOT(close()) );

    QGridLayout *lyt = new QGridLayout();

    lyt->addWidget( shellLabel,        0, 0, Qt::AlignLeft );
    lyt->addWidget( shellEdit,         0, 1, Qt::AlignRight );

    lyt->addWidget( comboLabel,        1, 0, Qt::AlignLeft );
    lyt->addWidget( colorSchemesCombo, 1, 1, Qt::AlignRight );

    lyt->addWidget( dspinLabel,        2, 0, Qt::AlignLeft );
    lyt->addWidget( transparencySpin,  2, 1, Qt::AlignRight );

    QHBoxLayout *fLyt = new QHBoxLayout();

    fLyt->addWidget( fontLabel );
    fLyt->addStretch();
    fLyt->addWidget( fontCombo );
    fLyt->addWidget( fontSizeSpin );
    lyt->addLayout( fLyt, 3, 0, 1, 2 );

    lyt->addWidget( cursorLabel,             4,  0, Qt::AlignLeft );
    lyt->addWidget( cursorShapeCombo,        4,  1, Qt::AlignRight );

    lyt->addWidget( termLabel,               5,  0, Qt::AlignLeft );
    lyt->addWidget( termEdit,                5,  1, Qt::AlignRight );

    lyt->addWidget( tabPosLabel,             6,  0, Qt::AlignLeft );
    lyt->addWidget( tabPositionCombo,        6,  1, Qt::AlignRight );

    lyt->addWidget( singleInstanceCheck,     7,  0, Qt::AlignLeft );
    lyt->addWidget( enableTransparencyCheck, 8,  0, Qt::AlignLeft );
    lyt->addWidget( borderlessCheck,         9,  0, Qt::AlignLeft );

    lyt->addWidget( okBtn,                   10, 1, Qt::AlignRight );

    setLayout( lyt );

    setWindowTitle( "DesQTerm - Settings" );
    setWindowIcon( QIcon::fromTheme( "desq-term" ) );
}


void SettingsDialog::setFont( QFont font ) {
    font.setPointSize( fontSizeSpin->value() );
    settings->setValue( "Font", font );
}


void SettingsDialog::setFont( int size ) {
    QFont font( fontCombo->currentFont() );

    font.setPointSize( size );

    settings->setValue( "Font", font );
}


void SettingsDialog::setEnableTransparency() {
    settings->setValue( "EnableTransparency", enableTransparencyCheck->isChecked() );
}


void SettingsDialog::setTransparency() {
    qreal value = transparencySpin->value();

    settings->setValue( "Transparency", value );
}


void SettingsDialog::setBorderless() {
    settings->setValue( "Borderless", borderlessCheck->isChecked() );
}


void SettingsDialog::setSingleInstance() {
    settings->setValue( "UseSingleInstance", singleInstanceCheck->isChecked() );
}
