/**
 * Copyright 2012-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <Global.hpp>

class TabWidget : public QTabWidget {
    Q_OBJECT

    public:
        TabWidget( QWidget *parent, bool mode = false );

        void removeTab( int );

    private:
        bool ddMode;
        void renameTab( QString, int );

    public slots:
        int newTerminal();
        int newTerminal( QString, QString cmd );
        int newTerminalCWD();
        void clearTerminal();
        void copyToClipboard();
        void pasteClipboard();

        void prevTerminal();
        void nextTerminal();

        void closeTab( int idx = -1 );

    private slots:
        void printSelection( bool );

    Q_SIGNALS:
        void close();
        void changeWindowTitle( QString );
};
