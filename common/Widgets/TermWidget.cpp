/**
 * Copyright 2012-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "TermWidget.hpp"

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

static QFileSystemWatcher *watcher = new QFileSystemWatcher();

QString getUserShell() {
    QString shell = settings->value( "Shell" );

    /** Retrieve the default shell from /etc/passwd */
    if ( shell == "default" ) {
        struct passwd *uInfo = getpwuid( geteuid() );
        shell = uInfo->pw_shell;
    }

    /** shell program does not exist => use /bin/bash */
    if ( not QFileInfo( shell ).exists() ) {
        shell = "/bin/bash";
    }

    return shell;
}


TermWidget::TermWidget( QWidget *parent ) : QTermWidget( 0, parent ) {
    basicInit();

    startShellProgram();

    watcher->addPath( QString( "/proc/%1/" ).arg( getShellPID() ) );
    oldCWD = QFileInfo( QString( "/proc/%1/cwd" ).arg( getShellPID() ) ).symLinkTarget();
    connect( watcher, &QFileSystemWatcher::directoryChanged, this, &TermWidget::handleFSWSignals );

    setFocus();
}


TermWidget::TermWidget( QString wDir, QWidget *parent ) : QTermWidget( 0, parent ) {
    basicInit();

    setWorkingDirectory( wDir );

    startShellProgram();
    watcher->addPath( QString( "/proc/%1/" ).arg( getShellPID() ) );
    oldCWD = QFileInfo( QString( "/proc/%1/cwd" ).arg( getShellPID() ) ).symLinkTarget();
    connect( watcher, &QFileSystemWatcher::directoryChanged, this, &TermWidget::handleFSWSignals );

    setFocus();
}


TermWidget::TermWidget( QString wDir, QString cmd, QWidget *parent ) : QTermWidget( 0, parent ) {
    basicInit();

    setWorkingDirectory( wDir );

    if ( not cmd.isEmpty() ) {
        setArgs( QStringList() << "-l" << "-c" << cmd );
    }

    startShellProgram();

    setFocus();
}


QString TermWidget::currentWorkingDirectory() {
    QString cwd = QString( "/proc/%1/cwd" ).arg( getShellPID() );

    return QFileInfo( cwd ).symLinkTarget();
}


QString TermWidget::title() {
    return mTitle;
}


bool TermWidget::hasForegroundProcess() {
    pid_t procPID = getForegroundProcessId();

    return ( (procPID != 0) and (procPID != getShellPID() ) );
}


void TermWidget::basicInit() {
    /* Set the enivronment variable TERM as xterm */
    QProcessEnvironment procEnv = QProcessEnvironment::systemEnvironment();

    procEnv.insert( "TERM", settings->value( "Term" ) );
    setEnvironment( procEnv.toStringList() );

    setColorScheme( settings->value( "ColorScheme" ) );
    setKeyBindings( settings->value( "KeyTab" ) );

    setScrollBarPosition( QTermWidget::NoScrollBar );

    setTerminalFont( settings->value( "Font" ) );
    setHistorySize( settings->value( "HistorySize" ) );
    setKeyboardCursorShape( ( QTermWidget::KeyboardCursorShape )( int )settings->value( "CursorShape" ) );

    setShellProgram( getUserShell() );

    setMotionAfterPasting( 2 );
    setFlowControlEnabled( true );
    setFlowControlWarningEnabled( true );

    setTerminalOpacity( 1 - ( float )settings->value( "Transparency" ) / 100.0 );

    /* Actions */
    copyAct = new QAction( QIcon::fromTheme( "edit-copy" ), "Copy", this );
    connect( copyAct,      SIGNAL( triggered() ), this, SLOT( copyClipboard() ) );

    pasteSelAct = new QAction( QIcon::fromTheme( "edit-paste" ), "Paste Selection", this );
    connect( pasteSelAct,  SIGNAL( triggered() ), this, SLOT( pasteSelection() ) );

    pasteClipAct = new QAction( QIcon::fromTheme( "edit-paste" ), "Paste Clipboard", this );
    connect( pasteClipAct, SIGNAL(triggered()),   this, SLOT( pasteClipboard() ) );

    clearAct = new QAction( QIcon::fromTheme( "edit-clear" ), "Clear Terminal", this );
    connect( clearAct,     SIGNAL( triggered() ), this, SLOT( clear() ) );

    searchAct = new QAction( QIcon::fromTheme( "edit-find" ), "Search Terminal", this );
    connect( searchAct,    SIGNAL( triggered() ), this, SLOT( toggleShowSearchBar() ) );

    zoomInAct = new QAction( QIcon::fromTheme( "zoom-In" ), "Zoom In", this );
    zoomInAct->setShortcut( QKeySequence::ZoomIn );
    connect( zoomInAct, &QAction::triggered, this, &QTermWidget::zoomIn );
    addAction( zoomInAct );

    zoomOutAct = new QAction( QIcon::fromTheme( "zoom-Out" ), "Zoom Out", this );
    zoomOutAct->setShortcut( QKeySequence::ZoomOut );
    connect( zoomOutAct, &QAction::triggered, this, &QTermWidget::zoomOut );
    addAction( zoomOutAct );

    resetZoomAct = new QAction( QIcon::fromTheme( "zoom-Out" ), "Reset Zoom", this );
    resetZoomAct->setShortcut( QKeySequence( Qt::CTRL | Qt::Key_0 ) );
    connect(
        resetZoomAct, &QAction::triggered, [ = ] () {
            setTerminalFont( settings->value( "Font" ) );
        }
    );
    addAction( resetZoomAct );

    setContextMenuPolicy( Qt::CustomContextMenu );
    connect( this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(showCustomContextMenu(const QPoint&)) );

    connect(
        this, &QTermWidget::titleChanged, [ = ] () mutable {
            mTitle = QTermWidget::title();
            emit titleChanged( mTitle );
        }
    );

    setConfirmMultilinePaste( false );
}


void TermWidget::reloadSettings( QString key, QVariant value ) {
    if ( key == "ColorScheme" ) {
        setColorScheme( value.toString() );
    }

    else if ( key == "KeyTab" ) {
        setKeyBindings( value.toString() );
    }

    else if ( key == "Font" ) {
        qDebug() << "Font changed:" << value;
        setTerminalFont( value.value<QFont>() );
    }

    else if ( key == "HistorySize" ) {
        setHistorySize( value.toInt() );
    }

    else if ( key == "CursorShape" ) {
        setKeyboardCursorShape( ( QTermWidget::KeyboardCursorShape )value.toInt() );
    }

    else if ( key == "Transparency" ) {
        setTerminalOpacity( 1.0 - value.toInt() / 100.0 );
    }
}


void TermWidget::handleFSWSignals( QString ) {
    if ( QFileInfo( QString( "/proc/%1/cwd" ).arg( getShellPID() ) ).symLinkTarget() == oldCWD ) {
        return;
    }

    oldCWD = QFileInfo( QString( "/proc/%1/cwd" ).arg( getShellPID() ) ).symLinkTarget();
    emit chDir( oldCWD );
}


void TermWidget::showCustomContextMenu( const QPoint& pos ) {
    QMenu *menu = new QMenu( this );

    menu->addAction( copyAct );
    menu->addAction( pasteSelAct );
    menu->addAction( pasteClipAct );
    menu->addSeparator();
    menu->addAction( clearAct );
    menu->addSeparator();
    menu->addAction( searchAct );
    menu->addSeparator();
    menu->addAction( zoomInAct );
    menu->addAction( zoomOutAct );

    menu->exec( mapToGlobal( pos ) );
}


void TermWidget::wheelEvent( QWheelEvent *wEvent ) {
    if ( wEvent->modifiers() & Qt::CTRL ) {
        QPoint numPixels = wEvent->pixelDelta();

        if ( numPixels.isNull() ) {
            QPointF numDegs = wEvent->angleDelta() / 8 / 15;
            for ( int i = 0; i < fabs( numDegs.y() ); i++ ) {
                if ( numDegs.y() > 0 ) {
                    zoomIn();
                }

                else {
                    zoomOut();
                }
            }
        }

        wEvent->accept();
        return;
    }

    QTermWidget::wheelEvent( wEvent );
}
