/**
 * Copyright 2012-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <Global.hpp>
#include <qtermwidget6/qtermwidget.h>

class TermWidget : public QTermWidget {
    Q_OBJECT

    public:
        TermWidget( QWidget *parent                   = 0 );
        TermWidget( QString, QWidget *parent          = 0 );
        TermWidget( QString, QString, QWidget *parent = 0 );

        QString currentWorkingDirectory();
        void reloadSettings( QString, QVariant );

        QString title();

        bool hasForegroundProcess();

    private:
        void basicInit();

        QString oldCWD;
        QString mTitle;
        QAction *copyAct, *pasteSelAct, *pasteClipAct, *clearAct, *searchAct;
        QAction *zoomInAct, *zoomOutAct, *resetZoomAct;

    private slots:
        void handleFSWSignals( QString );
        void showCustomContextMenu( const QPoint& );

    protected:
        void wheelEvent( QWheelEvent *wEvent ) override;

    Q_SIGNALS:
        void chDir( QString );
        void titleChanged( QString );
};
