/**
 * Copyright 2012-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "TabWidget.hpp"
#include "TermWidget.hpp"

TabWidget::TabWidget( QWidget *parent, bool mode ) : QTabWidget( parent ) {
    /* DropDownMode */
    ddMode = mode;

    setMovable( true );
    setTabsClosable( false );
    setDocumentMode( true );

    setFocusPolicy( Qt::NoFocus );

    connect( this, &QTabWidget::tabCloseRequested, this, &TabWidget::closeTab );

    if ( ddMode ) {
        setTabPosition( QTabWidget::South );
    }

    else {
        QVariant posVar = settings->value( "TabPosition" );
        setTabPosition( (QTabWidget::TabPosition)posVar.toInt() );
    }
}


void TabWidget::removeTab( int idx ) {
    TermWidget *widget = qobject_cast<TermWidget *>( currentWidget() );

    if ( widget and widget->hasForegroundProcess() ) {
        int reply = QMessageBox::question(
            this,
            "DesQ Term | Close Tab?",
            "You have a process running in the foreground. Closing this tab will kill the process. "
            "Kill the process and close tab?",
            QMessageBox::Yes | QMessageBox::No
        );

        if ( reply == QMessageBox::No ) {
            return;
        }
    }

    QTabWidget::removeTab( idx );
}


void TabWidget::renameTab( QString text, int idx ) {
    qDebug() << "New tab title:" << text;

    text = text.split( " ", Qt::SkipEmptyParts ).takeLast();
    emit changeWindowTitle( (ddMode ? "DesQ DropTerm | " : "DesQ Term | ") + text );

    if ( ddMode ) {
        setTabText( idx, text );
    }
}


int TabWidget::newTerminal() {
    TermWidget *widget = new TermWidget( this );

    widget->setFocusPolicy( Qt::StrongFocus );

    int idx = addTab( widget, QIcon::fromTheme( "desq-term" ), "" );

    connect( widget, SIGNAL(finished()), this, SLOT(closeTab()) );

    setCurrentIndex( idx );

    connect(
        widget, &TermWidget::titleChanged, [ = ]( QString newTitle ) {
            /** Calculate the position of the tab every single time */
            int idx = indexOf( widget );
            renameTab( newTitle, idx );
        }
    );

    widget->setFocus();

    return idx;
}


int TabWidget::newTerminal( QString wDir, QString cmd ) {
    TermWidget *widget = new TermWidget( wDir, cmd, this );
    int        idx     = addTab( widget, QIcon::fromTheme( "desq-term" ), "" );

    widget->setFocusPolicy( Qt::StrongFocus );
    connect(
        widget, &TermWidget::finished, [ = ] {
            closeTab();
        }
    );

    setCurrentIndex( idx );

    connect(
        widget, &TermWidget::titleChanged, [ = ]( QString newTitle ) {
            /** Calculate the position of the tab every single time */
            int idx = indexOf( widget );
            renameTab( newTitle, idx );
        }
    );

    widget->setFocus();

    return idx;
}


int TabWidget::newTerminalCWD() {
    TermWidget *curTerm = qobject_cast<TermWidget *>( currentWidget() );
    QString    cwd      = curTerm->currentWorkingDirectory();

    return newTerminal( cwd, "" );
}


void TabWidget::clearTerminal() {
    TermWidget *widget = qobject_cast<TermWidget *>( currentWidget() );

    widget->clear();
}


void TabWidget::copyToClipboard() {
    TermWidget *widget = qobject_cast<TermWidget *>( currentWidget() );

    widget->copyClipboard();
}


void TabWidget::pasteClipboard() {
    TermWidget *widget = qobject_cast<TermWidget *>( currentWidget() );

    widget->pasteClipboard();
}


void TabWidget::closeTab( int tabIndex ) {
    if ( tabIndex == -1 ) {
        if ( qobject_cast<TermWidget *>( sender() ) ) {
            removeTab( indexOf( qobject_cast<TermWidget *>( sender() ) ) );
        }

        else {
            removeTab( currentIndex() );
        }
    }

    else {
        removeTab( tabIndex );
    }

    if ( count() == 0 ) {
        if ( ddMode ) {
            newTerminal();
        }

        else {
            emit close();
        }
    }
}


void TabWidget::prevTerminal() {
    int idx = currentIndex();

    if ( idx == 0 ) {
        setCurrentIndex( count() - 1 );
    }

    else {
        setCurrentIndex( idx - 1 );
    }

    emit changeWindowTitle( (ddMode ? "DesQ DropTerm | " : "DesQ Term | ") + tabText( idx - 1 ) );
}


void TabWidget::nextTerminal() {
    int idx = currentIndex();

    if ( idx == count() - 1 ) {
        setCurrentIndex( 0 );
    }

    else {
        setCurrentIndex( idx + 1 );
    }

    emit changeWindowTitle( (ddMode ? "DesQ DropTerm | " : "DesQ Term | ") + tabText( idx + 1 ) );
}


void TabWidget::printSelection( bool selection ) {
    TermWidget *widget = qobject_cast<TermWidget *>( currentWidget() );

    widget->copyClipboard();

    if ( selection ) {
        widget->pasteSelection();
    }
}
